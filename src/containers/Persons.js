import React, { Component } from "react";
import { connect } from "react-redux";
import { addPerson, deletePerson } from "../Redux/actions/personsActions";

import Person from "../components/Person/Person";
import AddPerson from "../components/AddPerson/AddPerson";

class Persons extends Component {
  personAddedHandler = () => {
    return {
      id: Math.random(), // not really unique but good enough here!
      name: "Max",
      age: Math.floor(Math.random() * 40)
    };
  };

  render() {
    const persons = this.props.persons;
    return (
      <div>
        <AddPerson
          personAdded={() => this.props.addPerson(this.personAddedHandler())}
        />
        {persons.map(person => (
          <Person
            key={person.id}
            name={person.name}
            age={person.age}
            id={person.id}
            clicked={() => this.props.deletePerson(person)}
          />
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  persons: state.persons.persons
});

export default connect(
  mapStateToProps,
  { addPerson, deletePerson }
)(Persons);
