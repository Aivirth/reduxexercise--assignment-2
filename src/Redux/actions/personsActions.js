import * as types from "./types";

export const addPerson = personObj => {
  return {
    type: types.ADD_PERSON,
    payload: personObj
  };
};

export const deletePerson = personID => {
  return {
    type: types.DELETE_PERSON,
    payload: personID
  };
};
