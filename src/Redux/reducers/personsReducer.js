import * as types from "../actions/types";

const initialState = {
  persons: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.ADD_PERSON:
      return {
        ...state,
        persons: state.persons.concat(action.payload)
      };

    case types.DELETE_PERSON:
      const updatedPersons = state.persons.filter(
        person => person.id !== action.payload.id
      );

      return {
        ...state,
        persons: updatedPersons
      };

    default:
      return state;
  }
}
